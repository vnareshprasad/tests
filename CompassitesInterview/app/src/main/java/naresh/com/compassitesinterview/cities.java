package naresh.com.compassitesinterview;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Class cities
 *
 * Activity with 2 list views  City and States
 *
 *
 *
 * TODO: Flip independent list view items instead of whole list view
 * TODO: optimize the city and state in single list view
 *
 * @Author:Nareshprasad Venkatachalam
 */
public class cities extends Activity {

    private static final String TAG = cities.class.getName();
    private static final int DURATION = 1500;
    private SeekBar mSeekBar;

    //Map<String, String> cityState = new HashMap<String, String>();

    //TODO: convert the 2 list items to a Hash map with city-string key value pair to avoid entry mistakes
    private static final String[] LIST_STRINGS_CITY = new String[] {
            "Bangalore",
            "Ahmedabad",
            "Panaji",
            "Chennai",
            "Hyderabad",
            "Pune",
            "Mumbai"
    };

    private static final String[] LIST_STRINGS_STATE = new String[] {
            "Karnataka",
            "Gujarat",
            "Goa",
            "Tamil nadu",
            "Telangana",
            "Maharastra",
            "Maharastra"
    };

    ListView mCityList;
    ListView mStateList;
    private View viewCounter;
    ArrayAdapter adapterCities;
    ArrayAdapter adapterStates;
    List<CityState> cityStateList = new ArrayList<CityState>();

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cities);
        mCityList = (ListView) findViewById(R.id.list_cities);
        mStateList = (ListView) findViewById(R.id.list_states);


        for (int i = 0; i < LIST_STRINGS_CITY.length; i++) {
            CityState cityState = new CityState(LIST_STRINGS_CITY[i],
                    LIST_STRINGS_STATE[i]);
                cityStateList.add(cityState);
        }

        // Prepare the ListView
        adapterCities = new listAdapter(this, cityStateList);
        adapterStates = new listAdapter(this, cityStateList);
        mCityList.setTag(1);
        mStateList.setTag(2);

        // Prepare the ListView
        final ArrayAdapter<String> adapterFr = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, LIST_STRINGS_STATE);


        mCityList.setAdapter(adapterCities);

        mStateList.setAdapter(adapterStates);
        mStateList.setRotationY(-90f);

        Button btnFlip = (Button) findViewById(R.id.btnFlip);

        btnFlip.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.i(TAG, "btnFlip.setOnClickListener");
                flipit();
            }
        });

        Button btnAdd = (Button) findViewById(R.id.btnAdd);
        //Add a new city from available list for now.
        btnAdd.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int randCityNumber = (new Random().nextInt(LIST_STRINGS_CITY.length));
                CityState cityState = new CityState(LIST_STRINGS_CITY[randCityNumber],
                        LIST_STRINGS_STATE[randCityNumber]);
                adapterCities.add(cityState);
            }
        });
    }

    private Interpolator accelerator = new AccelerateInterpolator();
    private Interpolator decelerator = new DecelerateInterpolator();

    private void flipit() {
        final ListView visibleList;
        final ListView invisibleList;
        if (mCityList.getVisibility() == View.GONE) {
            visibleList = mStateList;
            invisibleList = mCityList;
        } else {
            invisibleList = mStateList;
            visibleList = mCityList;
        }
        ObjectAnimator visToInvis = ObjectAnimator.ofFloat(visibleList, "rotationY", 0f, 90f);
        visToInvis.setDuration(500);
        visToInvis.setInterpolator(accelerator);
        final ObjectAnimator invisToVis = ObjectAnimator.ofFloat(invisibleList, "rotationY",
                -90f, 0f);
        invisToVis.setDuration(500);
        invisToVis.setInterpolator(decelerator);
        visToInvis.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator anim) {
                visibleList.setVisibility(View.GONE);
                invisToVis.start();
                invisibleList.setVisibility(View.VISIBLE);
            }
        });
        visToInvis.start();
    }

    /**
     * ListAdapter - Custom list adapter extends arrayadapter
     *
     * List of cityState class
     * ViewHolder with Text view and image Delete
     *
     * Depend on tag city/state name is displayed
     *
     * Add and delete row methods.
     */
    public class listAdapter extends ArrayAdapter {
        Context mContext = null;
        List<CityState> mCityStateList;
        public listAdapter(Context context, List<CityState>  cityStateList) {
            super(context, R.layout.list_item, cityStateList);
            mContext = context;
            mCityStateList = cityStateList;
        }

        private class ViewHolder {
            TextView txtCity;
            ImageView imgDelete;
        }

        @Override
        public View getView(final int position, View convertView, final ViewGroup parent) {
            ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if(convertView == null) {
                convertView = mInflater.inflate(R.layout.list_item, null);
                holder = new ViewHolder();
                holder.txtCity = (TextView)convertView.findViewById(R.id.txtCity);
                holder.imgDelete = (ImageView)convertView.findViewById(R.id.imgDelete);
                holder.imgDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        remove(mCityStateList.get(position));
                    }
                });
                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            if(parent.getTag().equals(1)){
                holder.txtCity.setText(mCityStateList.get(position).getCity());
            }else{
                holder.txtCity.setText(mCityStateList.get(position).getState());
            }

            return convertView;
        }

        @Override
        public void add(Object object) {
            mCityStateList.add((CityState) object);
            notifyDataSetChanged();
        }

        @Override
        public void remove(Object object) {
            mCityStateList.remove(object);
            notifyDataSetChanged();
        }
    }

    /**
     * Class to have city and state
     * getters and setters
     */
    public class CityState {

        private String city;
        private String state;

        public CityState(String city, String state){
            this.city = city;
            this.state = state;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

    }
}
